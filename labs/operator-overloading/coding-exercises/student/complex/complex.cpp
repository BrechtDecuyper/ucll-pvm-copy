#include "complex.h"
#include "cmath"

double Complex::abs_sqr() const
{
	return (this->re * this->re + this->im * this->im);
}

double Complex::abs() const
{
	return sqrt(this->abs_sqr());
}

Complex Complex::conjugate() const
{
	return Complex(this->re, -this->im);
}

Complex operator"" _i(long long unsigned im)
{
	return Complex(0, (double)im);
}

Complex operator"" _i(long double im)
{
	return Complex(0, (double)im);
}

Complex operator +(const Complex& c1, const Complex& c2)
{
	return Complex(c1.re + c2.re, c1.im + c2.im);
}

Complex operator -(const Complex& c)
{
	return Complex(-c.re, -c.im);
}

Complex operator -(const Complex& c1, const Complex& c2)
{
	return Complex(c1.re - c2.re, c1.im - c2.im);
}

Complex operator *(const Complex& c1, const Complex& c2)
{
	return Complex(c1.re * c2.re - c1.im * c2.im, c1.re * c2.im + c2.re * c1.im);
}

Complex operator /(const Complex& c1, const Complex& c2)
{
	Complex c3 = c2 * c2.conjugate();

	return (c1 * c2.conjugate()) / c3.re;
}

Complex operator /(const Complex& c, double d)
{
	return Complex(c.re / d, c.im / d);
}

Complex& operator +=(Complex& c1, const Complex& c2)
{
	return (c1 = c1 + c2);
}

Complex& operator -=(Complex& c1, const Complex& c2)
{
	return (c1 = c1 - c2);
}

Complex& operator *=(Complex& c1, const Complex& c2)
{
	return (c1 = c1 * c2);
}

Complex& operator /=(Complex& c1, const Complex& c2)
{
	return (c1 = c1 / c2);
}

bool operator ==(const Complex& c1, const Complex& c2)
{
	return (c1.re == c2.re && c1.im == c2.im);
}

bool operator !=(const Complex& c1, const Complex& c2)
{
	return !(c1 == c2);
}
