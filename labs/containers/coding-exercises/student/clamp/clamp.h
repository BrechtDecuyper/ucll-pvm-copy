#ifndef CLAMP_H
#define CLAMP_H

#include <vector>


template<typename Iterator>
void clamp(Iterator start, Iterator end)
{
	for (Iterator i = start; i != end; i++)
	{
		if (*i < 0)
		{
			*i = 0;
		}
	}
}

#endif
