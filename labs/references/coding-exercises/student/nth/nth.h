#pragma once
#ifndef NTH_F
#define NTH_F

#include "../Catch.h"

int& nth(std::vector<int>& ns, int index);
int nth(const std::vector<int>& ns, int index);

#endif // !NTH_F