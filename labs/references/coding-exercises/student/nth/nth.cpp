#include "nth.h"

int& nth(std::vector<int>& ns, int index)
{
	index = index % int(ns.size());
	index += ns.size();
	index = index % int(ns.size());

	return ns[index];
}

int nth(const std::vector<int>& ns, int index)
{
	index = index % int(ns.size());
	index += ns.size();
	index = index % int(ns.size());

	return ns[index];
}