#include "linked_list.h"


/*
  Counts the number of elements in the list.

  The null pointer is written nullptr in C++.
*/
unsigned length(linked_list* lst)
{
	unsigned l = 0;
	while (lst != nullptr)
	{
		l++;
		lst = lst->next;
	}
	return l;
}

/*
  Finds the penultimate node in the list.
  The penultimate node is the node just before the last one.
  If there is no penultimate node (because the list is too short),
  return nullptr.
*/
linked_list* penultimate(linked_list* lst)
{
	if (length(lst) < 2)
	{
		return nullptr;
	}

	while (lst->next->next != nullptr)
	{
		lst = lst->next;
	}

	return lst;
}

/*
  Returns a pointer to the longest list.
  Keep it as efficient as possible: if you
  get a list of 1 long and one of 1 billion long,
  you do not want to spend time counting a billion elements.

  If both lists have equal length, return nullptr.
*/
linked_list* longest(linked_list* xs, linked_list* ys)
{
	linked_list* xp = xs;
	linked_list* yp = ys;

	while (xs != nullptr)
	{
		if (ys == nullptr)
		{
			return xp;
		}
		xs = xs->next;
		ys = ys->next;
	}

	if (ys == nullptr)
	{
		return nullptr;
	}
	else
	{
		return yp;
	}
}

/*
  Given a non-cyclic linked list, modifies
  it so as to make it cyclic by having
  the last node point to the first.
*/
void make_cyclic(linked_list* lst)
{
	linked_list* first = lst;

	while (lst->next != nullptr)
	{
		lst = lst->next;
	}

	lst->next = first;
}

/*
  Checks whether the linked list contains
  a cycle. You will not need a special
  data structure for this (i.e. no arrays,
  lists, sets, ... are necessary).
  If you need inspiration for how to do this,
  look up hares and turtoises.
*/
bool has_cycle(linked_list* lst)
{
	if (lst == nullptr)
	{
		return false;
	}
	else
	{
		linked_list* turtoise = lst;
		linked_list* hare = lst->next;

		while (turtoise != hare)
		{
			if (hare == nullptr || hare->next == nullptr)
			{
				return false;
			}
			else
			{
				turtoise = turtoise->next;
				hare = hare->next->next;
			}
		}

		return true;
	}
}
