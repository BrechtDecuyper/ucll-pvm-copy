#include "range.h"


int* range(int from, int to)
{
	int size = to - from + 1;
	int* result = new int[size];

	for (int i = 0; i != size; i++)
	{
		result[i] = from + i;
	}

	return result;
}
