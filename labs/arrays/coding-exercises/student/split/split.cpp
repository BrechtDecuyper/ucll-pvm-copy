#include "split.h"


void split(int* ns, unsigned size, int** odds, unsigned* n_odds, int** evens, unsigned* n_evens)
{
	*n_odds = *n_evens = 0;

	for (unsigned i = 0; i != size; ++i)
	{
		++*(ns[i] % 2 ? n_odds : n_evens);
	}

	*odds = new int[*n_odds];
	*evens = new int[*n_evens];

	unsigned j = 0, k = 0;
	for (unsigned i = 0; i != size; ++i)
	{
		if (ns[i] % 2 == 0)
		{
			(*evens)[j++] = ns[i];
		}
		else
		{
			(*odds)[k++] = ns[i];
		}
	}
}
