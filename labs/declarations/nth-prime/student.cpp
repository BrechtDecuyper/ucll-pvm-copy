#include <cmath>

bool is_prime(unsigned n);

// Start counting at 0: nth_prime(0) == 2
// (no, 1 is not a prime)
unsigned nth_prime(unsigned n)
{
	unsigned prime = 2;

	if (n == 0)
	{
		return prime;
	}
	else
	{
		while (n > 0)
		{
			prime++;
			if (is_prime(prime))
			{
				n--;
			}
		}

		return prime;
	}
}

bool is_prime(unsigned n)
{
	for (unsigned i = 2; i <= std::sqrt(n); i++)
	{
		if (n % i == 0)
		{
			return false;
		}
	}

	return true;
}